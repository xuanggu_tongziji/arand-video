using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
public class PlayerControler : MonoBehaviour
{
    // Start is called before the first frame update
    public VideoPlayer videoPlayer;
    public Sprite play;
    public Sprite pause;
    public string playindicate = "play";
    void Start()
    {
    }
    public void OnClick(Button button)
    {
        if (playindicate == "play")
        {
            playindicate = "pause";
            button.GetComponent<Image>().sprite = pause;
            videoPlayer.Play();
        }
        else if(playindicate == "pause")
        {
            playindicate = "play";
            button.GetComponent<Image>().sprite = play;
            videoPlayer.Pause();
        }


    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
