using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VideoProgress : MonoBehaviour,IDragHandler,IPointerDownHandler
{
    public VideoPlayer video;
    public Image progressBar;
    // Update is called once per frame
    void start()
    {

    }
    public void OnDrag(PointerEventData eventData)
    {
        Jump(eventData);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        Jump(eventData);
    }
    private void Jump(PointerEventData eventData)
    {
        //获取指针进度条的位置
        float percent;
        Camera cam=Camera.main;
        Vector2 localPosition;
        //把鼠标在屏幕的位置转换为在矩形框中的位置
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(
            progressBar.rectTransform, eventData.position, null, out localPosition))
           {
            //把本地位置转换成百分比
            percent = Mathf.InverseLerp(progressBar.rectTransform.rect.xMin, progressBar.rectTransform.rect.xMax, localPosition.x);
            //percent = (localPosition.x - progressBar.rectTransform.anchorMin.x) / (progressBar.rectTransform.anchorMax.x - progressBar.rectTransform.anchorMin.x);
            //用百分比控制视频的进度
            var jumpToFrame = video.frameCount * ((float)percent);
            Debug.Log("percent:"+percent+","+ "xMin:" + progressBar.rectTransform.rect.xMin + "," +
                "xMax:" + progressBar.rectTransform.rect.xMax + "," + "localPosition.x:" + localPosition.x);
            video.frame = (long)jumpToFrame;
            }
    }
    void Update()
    {
            float progress = (float)video.frame/video.frameCount;
            progressBar.fillAmount = progress;
    }
    
}
